# Description

Your start point is `<URL>`. Go as far as you can! 

# Writeup

<details>
<summary>Click to expand</summary>

Dirbusting challenge.

1. Find `robots.txt` on given site and get directory `N00n3_C4n_9U355_tH15_N4m3` from there. Go into this directory.
2. Discover `.DS_Store` file, parse it and get another directory `MY_5uP3r_53Cur3_53Cr37_f0LD3R`. Go into it.
3. In this directory find file 'Desktop.ini` and get directory `d0n7_70Uch_7H12` from there. Go into this directory.
4. Now you can see `flag.png` file is here but you can't access it. Find `Thumbs.db` file and extract `flag.png` preview from it.

</details>

# Build

```sh
docker build -t mctf-web-easy-02 .
```

# Run

```sh
docker run -d --rm --name mctf-web-easy-02 -p 80:80 mctf-web-easy-02
```

# Deploy

Copy `docker-compose.yml` to the server and run:

```sh
docker-compose up -d
```